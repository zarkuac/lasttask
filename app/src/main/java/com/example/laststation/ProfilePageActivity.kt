package com.example.laststation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_profile_page.*

class ProfilePageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_page)
        init()
        logout()
    }
    private fun init() {
        Glide.with(this).load("https://i.pinimg.com/originals/21/a6/a8/21a6a8307359a3c5977ed0321777f672.jpg").into(coverImage)
        Glide.with(this).load("https://d3j2s6hdd6a7rg.cloudfront.net/v2/uploads/media/default/0001/10/thumb_9228_default_news_size_5.jpeg").into(profileImage)
    }
    private fun logout(){
        logoutButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
