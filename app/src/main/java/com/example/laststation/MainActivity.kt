package com.example.laststation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {

        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        loginButton.setOnClickListener {
            login()
        }
        signupButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
        fbSignup.setOnClickListener {

        }
    }

    private fun login() {
        if (loginEmail.text.isEmpty() && loginPassword.text.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        } else {
            auth.signInWithEmailAndPassword(
                loginEmail.text.toString(),
                loginPassword.text.toString()
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Login", "signInWithEmail:success")
                        Toast.makeText(
                            baseContext, "Authentication Successful.",
                            Toast.LENGTH_SHORT
                        ).show()
                        val user = auth.currentUser
                        val intent = Intent(this, ProfilePageActivity::class.java)
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Login", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    // ...
                }
        }
    }
    private fun fbsignup(){

    }
}
