package com.example.laststation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.signupButton
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        signupButton2.setOnClickListener {
            signup()
        }
    }

    private fun signup() {
        if (newEmail.text.isEmpty() || newPassword.text.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        } else {
            auth.createUserWithEmailAndPassword(
                newEmail.text.toString(),
                newPassword.text.toString()
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("რეგისტრაცია", "createUserWithEmail:success")
                        Toast.makeText(
                            baseContext, "Registration Successful.",
                            Toast.LENGTH_SHORT
                        ).show()
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("რეგისტრაცია", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Registration failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    // ...
                }
        }
    }
}